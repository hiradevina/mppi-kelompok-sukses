'''
python manage.py runscript load_database.py
'''
from show_sentiment.models import PositiveWord, NegativeWord, TweetSentiment
import re
import tweepy
import os

access_token = os.environ.get('access_token')
access_token_secret = os.environ.get('access_token_secret')
API_key = os.environ.get('API_key')
API_secret_key = os.environ.get('API_secret_key')

with open('positif.txt') as finput:
    for line in finput:
        line = line.strip()
        newItem = PositiveWord(word=line)
        newItem.save()


with open('negatif.txt') as finput:
    for line in finput:
        line = line.strip()
        newItem = NegativeWord(word=line)
        newItem.save()


# Authenticate to Twitter
auth = tweepy.OAuthHandler(API_key, API_secret_key)
auth.set_access_token(access_token, access_token_secret)

# Create API object
api = tweepy.API(auth)
res = api.search(q='corona OR covid', lang='id', count=999, result_type='mixed', tweet_mode='extended')
max_id = min_id = res[0].id
for item in res:
    min_id = min(min_id, item.id)
    max_id = max(max_id, item.id)

while True:
    tmp_res = api.search(q='corona OR covid', lang='id', count=999 - len(res), result_type='mixed',
                         tweet_mode='extended', since_id=max_id)
    if tmp_res.__len__() == 0 or len(res) >= 999:
        break
    for item in tmp_res:
        min_id = min(min_id, item.id)
        max_id = max(max_id, item.id)
    res += tmp_res

while True:
    tmp_res = api.search(q='corona OR covid', lang='id', count=999 - len(res), result_type='mixed',
                         tweet_mode='extended', max_id=min_id - 1)
    if tmp_res.__len__() == 0 or len(res) >= 999:
        break
    for item in tmp_res:
        min_id = min(min_id, item.id)
        max_id = max(max_id, item.id)
    res += tmp_res
tweets = list(map(lambda x: x.full_text.lower(), res))
tweet_ids = list(map(lambda x: x.id, res))


res = []
for tweet in tweets:
    res.append(re.sub(r'^ +| +$', '', re.sub(r'[^a-z]+', ' ', tweet)))

tweets = res


for idx, tweet in enumerate(tweets):
    score = 0
    for token in tweet.split(' '):
        if PositiveWord.objects.filter(word=token).exists():
            score += 1
        if NegativeWord.objects.filter(word=token).exists():
            score -= 1
    newTweetSentiment = TweetSentiment(tweet_id=tweet_ids[idx], sentiment_score=score)
    newTweetSentiment.save()
    if idx % 32 == 0:
        print(idx)
