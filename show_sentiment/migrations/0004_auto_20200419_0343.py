# Generated by Django 2.1.1 on 2020-04-19 03:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('show_sentiment', '0003_tweetsentiment'),
    ]

    operations = [
        migrations.DeleteModel(
            name='SentimentAnalysisResult',
        ),
        migrations.AlterField(
            model_name='tweetsentiment',
            name='tweet_id',
            field=models.BigIntegerField(primary_key=True, serialize=False),
        ),
    ]
