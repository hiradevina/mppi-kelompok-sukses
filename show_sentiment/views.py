from django.shortcuts import render, redirect
from .models import PositiveWord, NegativeWord, TweetSentiment

import os
import tweepy
import re


def update_context_index(context, request):
    if request.GET.get('update'):
        collect_latest_tweet()
    context.update({
        'positive_tweet': 0,
        'negative_tweet': 0,
        'unknown_tweet': 0,
        'total_tweet': 0
    })
    for tweet in TweetSentiment.objects.all().order_by('-tweet_id')[:999]:
        if tweet.sentiment_score > 0:
            context['positive_tweet'] += 1
        if tweet.sentiment_score < 0:
            context['negative_tweet'] += 1
        if tweet.sentiment_score == 0:
            context['unknown_tweet'] += 1
        context['total_tweet'] += 1


def dispatch_url(request, page):
    if not page.endswith('.html') or page not in os.listdir('show_sentiment/templates'):
        return render(request, '404.html')
    context = {}
    if page == 'index.html':
        update_context_index(context, request)
    return render(request, page, context)


def dispatch_index(request):
    return redirect('/index.html')


access_token = os.environ.get('access_token')
access_token_secret = os.environ.get('access_token_secret')
API_key = os.environ.get('API_key')
API_secret_key = os.environ.get('API_secret_key')


def collect_latest_tweet():
    # Authenticate to Twitter
    auth = tweepy.OAuthHandler(API_key, API_secret_key)
    auth.set_access_token(access_token, access_token_secret)

    # Create API object
    api = tweepy.API(auth)
    last_tweet_id = TweetSentiment.objects.last().tweet_id

    while True:
        tmp_res = api.search(q='corona OR covid', lang='id', count=100, result_type='mixed',
                             tweet_mode='extended', since_id=last_tweet_id)
        if tmp_res.__len__() == 0:
            break
        for item in tmp_res:
            last_tweet_id = max(last_tweet_id, item.id)
        tweet_ids = list(map(lambda x: x.id, tmp_res))
        tmp_res = list(map(lambda x: x.full_text.lower(), tmp_res))
        for i, tweet in enumerate(tmp_res):
            tmp_res[i] = re.sub(r'^ +| +$', '', re.sub(r'[^a-z]+', ' ', tweet))

        for idx, tweet in enumerate(tmp_res):
            score = 0
            for token in tweet.split(' '):
                if PositiveWord.objects.filter(word=token).exists():
                    score += 1
                if NegativeWord.objects.filter(word=token).exists():
                    score -= 1
            newTweetSentiment = TweetSentiment(tweet_id=tweet_ids[idx], sentiment_score=score)
            newTweetSentiment.save()
