from django.contrib import admin
from .models import PositiveWord, NegativeWord, TweetSentiment

# Register your models here.
admin.site.register(PositiveWord)
admin.site.register(NegativeWord)
admin.site.register(TweetSentiment)
