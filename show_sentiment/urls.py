from django.urls import re_path, path
from .views import dispatch_url, dispatch_index
# url for app
urlpatterns = [
    path('<str:page>', dispatch_url, name='dispatch_url'),
    re_path(r'^$', dispatch_index, name='dispatch_index')
]
